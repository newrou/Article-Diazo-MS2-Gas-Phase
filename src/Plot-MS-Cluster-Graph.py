#!/usr/bin/env python3
#from pyopenms import *
#import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors

def plot_ms(name, title, ms):
#     for int, ind in zip():
#         plt.plot([0, i], [mz, mz], color = 'black')
#         if i>7.0 : plt.text(i+2, mz, "%.4f" % mz)
     ind = 1+np.arange(8)
     plt.bar(ind, ms[name], width=0.37)
     plt.title('$\\mathbf{' + name + '}$   ' + title[name])
#     plt.xlabel('n')
#     plt.ylabel('Ambolance, abs.')
#     plt.ylim(bottom=0)
#     plt.xlim(left=0, right=119)

title = dict()
title['1a']='$C_6H_5N_2^+ TfO^-$'
title['1b']='2-$NO_2C_6H_4N_2^+ TfO^-$'
title['1c']='3-$NO_2C_6H_4N_2^+ TfO^-$'
title['1d']='4-$NO_2C_6H_4N_2^+ TfO^-$'
title['1e']='4-$MeOC_6H_4N_2^+ TfO^-$'
title['1f']='2-$HCO_2C_6H_4N_2^+ TfO^-$'
title['1g']='4-$HCO_2C_6H_4N_2^+ TfO^-$'
title['1h']='4-$BrC_6H_4N_2^+ TfO^-$'
title['1i']='4-Br-2-$HCO_2C_6H_4N_2^+ TfO^-$'
title['2']='4-$NO_2C_6H_4N_2^+ TsO^-$'
title['3']='4-$NO_2C_6H_4N_2^+ BF_4^-$'

ms = dict()
ms['1a']=[16677, 2383, 6634, 4252, 5736, 17022, 705, 0]
ms['1b']=[318437, 35066, 82974, 94245, 16912, 9680, 0, 0]
ms['1c']=[242321, 110927, 198494, 136849, 41087, 3478, 0, 0]
ms['1d']=[340795, 95220, 206412, 141580, 0, 0, 0, 0]
ms['1e']=[116628, 22493, 94173, 54904, 36505, 4199, 0, 0]
ms['1f']=[1325, 426, 0, 332, 4005, 5243, 1193, 3404]
ms['1g']=[5644, 0, 128, 0, 0, 0, 0, 0]
ms['1h']=[12809, 335, 0, 0, 0, 0, 0, 0]
ms['1i']=[2018, 2600, 664, 0, 0, 0, 0, 0]
ms['2']=[205817, 413793, 553818, 242131, 99595, 0, 0, 0]
ms['3']=[24953, 267, 115, 25822, 5098, 1935, 1171, 0]

fig=plt.figure(figsize=(15,11))

#plt.subplot(1,1,1)
#plt.xlabel('n')
#plt.ylabel('Ambulance, abs.')

plt.subplot(4,3,1)
plot_ms('1a', title, ms)

plt.subplot(4,3,2)
plot_ms('1b', title, ms)

plt.subplot(4,3,3)
plot_ms('1c', title, ms)

plt.subplot(4,3,4)
plot_ms('1d', title, ms)
plt.ylabel('Ambolance, abs.')

plt.subplot(4,3,5)
plot_ms('1e', title, ms)

plt.subplot(4,3,6)
plot_ms('1f', title, ms)

plt.subplot(4,3,7)
plot_ms('1g', title, ms)

plt.subplot(4,3,8)
plot_ms('1h', title, ms)

plt.subplot(4,3,9)
plot_ms('1i', title, ms)

plt.subplot(4,3,10)
plot_ms('2', title, ms)

plt.subplot(4,3,11)
plot_ms('3', title, ms)
plt.xlabel('Cluster size (n)')

#plt.suptitle('Cluster size (n)', fontweight ="bold", verticalalignment="bottom")
plt.tight_layout()
#plt.text(0, 0, 'common xlabel', ha='center', va='center')
#plt.text(0, 0, 'common ylabel', ha='center', va='center', rotation='vertical')
plt.savefig('MS-DS-Cluster-Diagram.png', dpi=300)

