#set terminal postscript eps
set terminal postscript eps color
set key inside right top vertical Right noreverse enhanced autotitles box linetype -1 linewidth 0.200
#set title "Integrate MS2 [6-CH3C5H3NO-4-N2+ TfO-] 1.00 eV"
set title "MS2 fragmentation M/Z=122.04 Ecolision=1.00 eV"
set ylabel "Ambulanse, abs." font "Helvetica-Bold,28"
set y2label "Ambulanse, %" font "Helvetica-Bold,28"
set xlabel "M/Z, a.e.m." font "Helvetica-Bold,28"
set bars small
#set xrange [`echo $MZRANGE`]
set xrange [0:160]
set yrange [0:120]
set y2range [0:110]
set y2tics 20
set ytics nomirror
#set bar 0
#set size 0.5,0.5
#set terminal postscript enhanced "Courier" 20

set terminal svg size 1200,900 font "Helvetica,28"
set key autotitle columnhead
set datafile separator ";"

set termoption dash

set linestyle 1 lt 1 lw 3 lc -1 dashtype 1
set linestyle 2 lt 2 lw 3 lc -1 dashtype 2
set linestyle 3 lt 3 lw 3 lc -1 dashtype 3
set linestyle 4 lt 4 lw 2 lc -1 dashtype 4
set linestyle 5 lt 5 lw 2 lc -1 dashtype 5
set linestyle 6 lt 6 lw 2 lc -1 dashtype 6
set linestyle 7 lt 7 lw 2 lc -1 dashtype 7
set linestyle 8 lt 8 lw 2 lc -1 dashtype 8
set linestyle 9 lt 9 lw 2 lc -1 dashtype 9

set style arrow 1 heads filled size screen 0.008,20,30 ls 1 lw 0.5
set style arrow 2 nohead ls 2 lw 0.8
set style arrow 3 head filled size screen 0.008,30,60 ls 1 lw 2.0

unset key
#set key outside
set nokey

set arrow from 122,20 to 94,20 as 3
set label "-28 [N_2]" at 100,25 center font "Helvetica,28"
set label "94.03" at 94,112 center font "Helvetica,28"
set label "122.04" at 122,38 center font "Helvetica,28"

set output 'ms2.svg'
plot 'ms2.dat' using 2:($3/1000.0) with impulses lw 5 lc -1, \
 'ms2.dat' using 2:4 with impulses lw 5 lc -1 axes x1y2

quit
