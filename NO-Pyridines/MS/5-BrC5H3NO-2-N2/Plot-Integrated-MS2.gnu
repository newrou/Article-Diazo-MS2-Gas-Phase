#set terminal postscript eps
set terminal postscript eps color
set key inside right top vertical Right noreverse enhanced autotitles box linetype -1 linewidth 0.200
#set title "Integrate MS2 [6-CH3C5H3NO-2-N2+ TfO-] 1.00 eV"
set title "MS2 fragmentation M/Z=199.95 Ecolision=1.00 eV"
set ylabel "Ambulanse, abs." font "Helvetica-Bold,28"
set y2label "Ambulanse, %" font "Helvetica-Bold,28"
set xlabel "M/Z, a.e.m." font "Helvetica-Bold,28"
set bars small
#set xrange [`echo $MZRANGE`]
set xrange [0:250]
set yrange [0:390]
set y2range [0:115]
set y2tics 20
set ytics nomirror
#set bar 0
#set size 0.5,0.5
#set terminal postscript enhanced "Courier" 20

set terminal svg size 1200,900 font "Helvetica,28"
set key autotitle columnhead
set datafile separator ";"

set termoption dash

set linestyle 1 lt 1 lw 3 lc -1 dashtype 1
set linestyle 2 lt 2 lw 3 lc -1 dashtype 2
set linestyle 3 lt 3 lw 3 lc -1 dashtype 3
set linestyle 4 lt 4 lw 2 lc -1 dashtype 4
set linestyle 5 lt 5 lw 2 lc -1 dashtype 5
set linestyle 6 lt 6 lw 2 lc -1 dashtype 6
set linestyle 7 lt 7 lw 2 lc -1 dashtype 7
set linestyle 8 lt 8 lw 2 lc -1 dashtype 8
set linestyle 9 lt 9 lw 2 lc -1 dashtype 9

set style arrow 1 heads filled size screen 0.008,20,30 ls 1 lw 0.5
set style arrow 2 nohead ls 2 lw 0.8
set style arrow 3 head filled size screen 0.008,30,60 ls 1 lw 2.0

unset key
#set key outside
set nokey

set arrow from 200,50 to 159,50 as 3
set label "-44 [N_2O]" at 167,65 center font "Helvetica,28"
set label "155.94" at 156,355 center font "Helvetica,28"
set label "199.95" at 200,97 center font "Helvetica,28"

set output 'ms2.svg'
plot 'ms2.dat' using 2:($3/1000.0) with impulses lw 5 lc -1, \
 'ms2.dat' using 2:4 with impulses lw 5 lc -1 axes x1y2

quit
