#set terminal postscript eps
set terminal postscript eps color
set key inside right top vertical Right noreverse enhanced autotitles box linetype -1 linewidth 0.200
#set title "Integrate MS Spectr"
set ylabel "Ambulanse, abs." font "Helvetica-Bold,22"
#set y2label "Ambulanse, %" font "Helvetica-Bold,22"
set xlabel "M/Z, a.e.m." font "Helvetica-Bold,22"
set bars small
#set xrange [`echo $MZRANGE`]
set xrange [0:700]
set yrange [0:20000]
#set y2range [0:100]
#set y2tics 10
#set size 0.5,0.5
#set terminal postscript enhanced "Courier" 20
set xtics axis nomirror out scale 0.5
set ytics axis nomirror out scale 0.5

set terminal svg size 1800,900 font "Helvetica,28"
set key autotitle columnhead
set datafile separator ";"

set termoption dash

set linestyle 1 lt 1 lw 3 lc -1 dashtype 1
set linestyle 2 lt 2 lw 3 lc -1 dashtype 2
set linestyle 3 lt 3 lw 3 lc -1 dashtype 3
set linestyle 4 lt 4 lw 2 lc -1 dashtype 4
set linestyle 5 lt 5 lw 2 lc -1 dashtype 5
set linestyle 6 lt 6 lw 2 lc -1 dashtype 6
set linestyle 7 lt 7 lw 2 lc -1 dashtype 7
set linestyle 8 lt 8 lw 2 lc -1 dashtype 8
set linestyle 9 lt 9 lw 2 lc -1 dashtype 9

set style arrow 1 heads filled size screen 0.008,20,30 ls 1 lw 0.5
set style arrow 2 nohead ls 2 lw 0.8
set style arrow 3 head filled size screen 0.008,20,30 ls 1 lw 0.5

unset key
#set key outside
set nokey

set output 'tmp-ms.svg'

set arrow from 172,0 to 172,85000 as 2
set label "(B)" at 72,190 center font "Helvetica,20"
set arrow from 172,0 to 172,85000 as 2
set label "(A)" at 145,340 center font "Helvetica,20"
set arrow from 393,0 to 393,850 as 2
set label "(C1)" at 423,730 center font "Helvetica,20"
set arrow from 664,0 to 664,850 as 2
set label "(C2)" at 694,255 center font "Helvetica,20"
set arrow from 935,0 to 935,850 as 2
#set label "(C3)" at 964,278 center font "Helvetica,20"
#set arrow from 1205,0 to 1205,850 as 2
#set label "(C4)" at 1235,180 center font "Helvetica,20"
#set arrow from 1477,0 to 1477,850 as 2
#set label "(C5)" at 1507,130 center font "Helvetica,20"
#set arrow from 1747,0 to 1747,850 as 2
#set label "(C6)" at 1777,90 center font "Helvetica,20"

set arrow from 122,500 to 94,500 as 3
set label "-[N_2]" at 130,520 center font "Helvetica,20"

#set arrow from 122,600 to 106,600 as 3
#set label "-[O]" at 150,620 center font "Helvetica,20"

set arrow from 122,770 to 393,770 as 3
#set label "+299 [M]" at 300,630 center font "Helvetica,20"
set label "+[C_5H_4NON_2^+ TfO^-]" at 145,800 center font "Helvetica,20"

set arrow from 393,770 to 664,770 as 3
set label "+[C_5H_4NON_2^+ TfO^-]" at 416,800 center font "Helvetica,20"

set arrow from 664,770 to 935,770 as 3
set label "+[C_5H_4NON_2^+ TfO^-]" at 687,800 center font "Helvetica,20"

set arrow from 935,770 to 1206,770 as 3
set label "+[C_5H_4NON_2^+ TfO^-]" at 957,800 center font "Helvetica,20"

set arrow from 1206,770 to 1477,770 as 3
set label "+[C_5H_4NON_2^+ TfO^-]" at 1229,800 center font "Helvetica,20"

set arrow from 1477,770 to 1748,770 as 3
set label "+[C_5H_4NON_2^+ TfO^-]" at 1500,800 center font "Helvetica,20"

plot 'tmp-ms.dat' using 2:($3/1000) with impulses lw 5 lc -1

quit
