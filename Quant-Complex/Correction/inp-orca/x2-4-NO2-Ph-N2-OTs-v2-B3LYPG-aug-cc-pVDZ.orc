! B3LYP/G aug-cc-pVDZ TightSCF TightOpt NumFreq Grid7
%geom MaxIter 2000 TolE 1e-8 TolRMSG 5e-7 TolMaxG 1e-6 Convergence tight end
%scf MaxIter 2000 end
%freq Temp 298.15, 323.15, 373.15, 423.15, 473.15, 523.15, 573.15, 623.15 end
* xyz 1 1
C          -3.90120        -0.78100        -0.21050
C          -4.52180        -0.01180         0.78640
C          -5.82050        -0.23920         1.27590
C          -6.52720        -1.30520         0.74060
C          -5.91990        -2.07910        -0.25000
C          -4.63630        -1.83950        -0.73400
H          -2.89570        -0.52250        -0.54420
H          -6.24670         0.39370         2.04580
H          -7.53030        -1.54580         1.06940
H          -4.22920        -2.47730        -1.50860
N          -3.81010         1.05000         1.32830
N          -3.17950         1.86370         1.76060
S          -0.35550         1.05980         0.27360
O          -0.92410         1.07200         1.65510
O           0.88090         0.17880         0.23720
O          -1.34140         0.70070        -0.78000
H           2.52530        -0.72140         1.37530
C           3.46250        -1.19050         1.09130
C           3.79130        -1.30080        -0.27480
C           4.37640        -1.69810         2.00870
C           4.97010        -1.88580        -0.75000
C           5.56710        -2.29010         1.57680
H           4.16000        -1.63210         3.06900
C           5.84010        -2.37240         0.21100
H           5.20730        -1.96260        -1.80400
N           2.88760        -0.79420        -1.20000
N           1.97750        -0.29910        -1.63780
N          -6.68960        -3.21950        -0.81900
O          -6.13260        -3.88540        -1.68300
O          -7.81740        -3.39980        -0.37580
C           0.19420         2.72360        -0.08460
C           0.68070         3.52500         0.95210
C           0.16760         3.19470        -1.39760
C           1.14640         4.80390         0.66140
H           0.67130         3.15480         1.97160
C           0.63910         4.47840        -1.67030
H          -0.23750         2.57130        -2.18730
C           1.13910         5.30070        -0.65170
H           1.51540         5.43220         1.46770
H           0.60960         4.85060        -2.69060
C           1.66420         6.68310        -0.95200
H           2.75860         6.70850        -0.89020
H           1.38220         7.01130        -1.95540
H           1.28520         7.41770        -0.23480
H           6.75510        -2.82810        -0.10530
N           6.54740        -2.83270         2.52840
O           6.30570        -2.75840         3.70070
O           7.54970        -3.32810         2.09430

*
%pal nprocs 42 end
