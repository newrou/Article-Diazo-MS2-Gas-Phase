#------------------------------------------------------------------------------
#$Date: 2016-02-18 17:37:37 +0200 (Thu, 18 Feb 2016) $
#$Revision: 176729 $
#$URL: svn://www.crystallography.net/cod/cif/1/50/48/1504815.cif $
#------------------------------------------------------------------------------
#
# This file is available in the Crystallography Open Database (COD),
# http://www.crystallography.net/
#
# All data on this site have been placed in the public domain by the
# contributors.
#
data_1504815
loop_
_publ_author_name
'Filimonov, Victor D.'
'Trusova, Marina'
'Postnikov, Pavel'
'Krasnokutskaya, Elena A.'
'Lee, Young Min'
'Hwang, Ho Yun'
'Kim, Hyunuk'
'Chi, Ki-Whan'
_publ_section_title
;
 Unusually stable, versatile, and pure arenediazonium tosylates: their
 preparation, structures, and synthetic applicability.
;
_journal_issue                   18
_journal_name_full               'Organic letters'
_journal_page_first              3961
_journal_page_last               3964
_journal_paper_doi               10.1021/ol8013528
_journal_volume                  10
_journal_year                    2008
_chemical_formula_moiety         'C6 H4 N2 I, C7 H7 O3 S'
_chemical_formula_sum            'C13 H11 I N2 O3 S'
_chemical_formula_weight         402.20
_chemical_name_common            'p-iodobenzenediazonium tosylate (15a)'
_chemical_name_systematic
; 
 ? 
;
_space_group_IT_number           2
_symmetry_cell_setting           triclinic
_symmetry_space_group_name_Hall  '-P 1'
_symmetry_space_group_name_H-M   'P -1'
_atom_sites_solution_hydrogens   geom
_atom_sites_solution_primary     direct
_atom_sites_solution_secondary   difmap
_audit_creation_method           SHELXL-97
_cell_angle_alpha                77.15(3)
_cell_angle_beta                 88.73(3)
_cell_angle_gamma                77.04(3)
_cell_formula_units_Z            4
_cell_length_a                   7.6780(15)
_cell_length_b                   10.149(2)
_cell_length_c                   18.657(4)
_cell_measurement_reflns_used    4411
_cell_measurement_temperature    90(2)
_cell_measurement_theta_max      30.35
_cell_measurement_theta_min      2.93
_cell_volume                     1380.7(5)
_computing_cell_refinement       HKL2000
_computing_data_collection       'Quantum 210'
_computing_data_reduction        HKL2000
_computing_molecular_graphics    'Materials Studio 4.1'
_computing_publication_material  'Materials Studio 4.1'
_computing_structure_refinement  'SHELXL-97 (Sheldrick, 1997)'
_computing_structure_solution    'SHELXS-97 (Sheldrick, 1990)'
_diffrn_ambient_temperature      90(2)
_diffrn_measured_fraction_theta_full 0.755
_diffrn_measured_fraction_theta_max 0.755
_diffrn_measurement_device_type  'Quantum 210'
_diffrn_measurement_method       \j-scan
_diffrn_radiation_monochromator  graphite
_diffrn_radiation_source         PAL_4A
_diffrn_radiation_type           'synchrotron radiation'
_diffrn_radiation_wavelength     0.80000
_diffrn_reflns_av_R_equivalents  0.0000
_diffrn_reflns_av_sigmaI/netI    0.0270
_diffrn_reflns_limit_h_max       8
_diffrn_reflns_limit_h_min       0
_diffrn_reflns_limit_k_max       12
_diffrn_reflns_limit_k_min       -12
_diffrn_reflns_limit_l_max       23
_diffrn_reflns_limit_l_min       -23
_diffrn_reflns_number            4411
_diffrn_reflns_theta_full        30.35
_diffrn_reflns_theta_max         30.35
_diffrn_reflns_theta_min         2.93
_exptl_absorpt_coefficient_mu    3.367
_exptl_absorpt_correction_T_max  0.8497
_exptl_absorpt_correction_T_min  0.6881
_exptl_absorpt_correction_type   empirical
_exptl_absorpt_process_details   HKL2000
_exptl_crystal_colour            colorless
_exptl_crystal_density_diffrn    1.935
_exptl_crystal_density_method    'not measured'
_exptl_crystal_description       block
_exptl_crystal_F_000             784
_exptl_crystal_size_max          0.12
_exptl_crystal_size_mid          0.10
_exptl_crystal_size_min          0.05
_refine_diff_density_max         1.844
_refine_diff_density_min         -1.998
_refine_diff_density_rms         0.346
_refine_ls_extinction_coef       0.017(2)
_refine_ls_extinction_expression Fc^*^=kFc[1+0.001xFc^2^\l^3^/sin(2\q)]^-1/4^
_refine_ls_extinction_method     SHELXL
_refine_ls_goodness_of_fit_ref   1.041
_refine_ls_hydrogen_treatment    constr
_refine_ls_matrix_type           full
_refine_ls_number_parameters     362
_refine_ls_number_reflns         4411
_refine_ls_number_restraints     0
_refine_ls_restrained_S_all      1.041
_refine_ls_R_factor_all          0.0814
_refine_ls_R_factor_gt           0.0793
_refine_ls_shift/su_max          0.000
_refine_ls_shift/su_mean         0.000
_refine_ls_structure_factor_coef Fsqd
_refine_ls_weighting_details
'calc w=1/[\s^2^(Fo^2^)+(0.1958P)^2^+3.6121P] where P=(Fo^2^+2Fc^2^)/3'
_refine_ls_weighting_scheme      calc
_refine_ls_wR_factor_gt          0.2202
_refine_ls_wR_factor_ref         0.2253
_reflns_number_gt                4189
_reflns_number_total             4411
_reflns_threshold_expression     >2sigma(I)
_cod_data_source_file            ol8013528_si_001.cif
_cod_data_source_block           15a
_cod_depositor_comments
;
The following automatic conversions were performed:

'_symmetry_cell_setting' value 'Triclinic' changed to 'triclinic'
according to /home/saulius/struct/CIF-dictionaries/cif_core.dic
dictionary named 'cif_core.dic' version 2.4.1 from 2010-06-29.

Automatic conversion script
Id: cif_fix_values 1715 2011-07-08 13:25:40Z adriana 
;
_cod_original_sg_symbol_H-M      P-1
_cod_database_code               1504815
loop_
_symmetry_equiv_pos_as_xyz
'x, y, z'
'-x, -y, -z'
loop_
_atom_site_label
_atom_site_type_symbol
_atom_site_fract_x
_atom_site_fract_y
_atom_site_fract_z
_atom_site_U_iso_or_equiv
_atom_site_adp_type
_atom_site_occupancy
_atom_site_symmetry_multiplicity
_atom_site_calc_flag
_atom_site_refinement_flags
I1 I 0.89096(8) 0.41768(4) 0.11180(2) 0.0244(3) Uani 1 1 d .
I2 I 0.04383(7) 0.58614(4) 0.38868(2) 0.0225(3) Uani 1 1 d .
S1 S 0.2565(3) 1.04661(17) 0.12787(9) 0.0250(5) Uani 1 1 d .
S2 S 0.7973(3) -0.04562(17) 0.36703(9) 0.0255(5) Uani 1 1 d .
O6 O 0.8450(13) -0.0398(6) 0.2906(3) 0.059(3) Uani 1 1 d .
O5 O 0.6454(10) -0.1052(5) 0.3886(4) 0.0507(18) Uani 1 1 d .
O4 O 0.9503(8) -0.1105(5) 0.4172(3) 0.0301(13) Uani 1 1 d .
O3 O 0.3734(8) 1.1022(5) 0.0730(2) 0.0289(11) Uani 1 1 d .
O2 O 0.0667(9) 1.1104(5) 0.1105(3) 0.0384(14) Uani 1 1 d .
O1 O 0.3112(9) 1.0444(5) 0.2025(3) 0.0389(15) Uani 1 1 d .
N1 N 0.7234(9) 1.0237(6) 0.1471(3) 0.0248(14) Uani 1 1 d .
N2 N 0.6941(10) 1.1338(6) 0.1515(3) 0.0314(16) Uani 1 1 d .
N4 N 0.2683(9) -0.0233(6) 0.3549(3) 0.0237(14) Uani 1 1 d .
N3 N 0.3054(9) -0.1301(6) 0.3478(3) 0.0280(15) Uani 1 1 d .
C1 C 0.8245(11) 0.6234(7) 0.1233(4) 0.0224(15) Uani 1 1 d .
C2 C 0.8728(11) 0.7255(7) 0.0670(4) 0.0281(17) Uani 1 1 d .
H2 H 0.9278 0.7013 0.0241 0.034 Uiso 1 1 calc R
C3 C 0.8390(11) 0.8624(7) 0.0748(4) 0.0250(15) Uani 1 1 d .
H3 H 0.8687 0.9342 0.0378 0.030 Uiso 1 1 calc R
C4 C 0.7597(10) 0.8885(7) 0.1396(3) 0.0236(15) Uani 1 1 d .
C5 C 0.7118(11) 0.7875(7) 0.1961(3) 0.0279(16) Uani 1 1 d .
H5 H 0.6579 0.8111 0.2392 0.034 Uiso 1 1 calc R
C6 C 0.7448(12) 0.6553(7) 0.1873(3) 0.0277(16) Uani 1 1 d .
H6 H 0.7138 0.5844 0.2245 0.033 Uiso 1 1 calc R
C7 C 0.1224(10) 0.3779(6) 0.3789(3) 0.0210(14) Uani 1 1 d .
C8 C 0.2223(11) 0.2800(7) 0.4350(3) 0.0237(15) Uani 1 1 d .
H8 H 0.2564 0.3055 0.4777 0.028 Uiso 1 1 calc R
C9 C 0.2722(11) 0.1430(7) 0.4277(3) 0.0251(16) Uani 1 1 d .
H9 H 0.3400 0.0723 0.4653 0.030 Uiso 1 1 calc R
C10 C 0.2181(11) 0.1135(7) 0.3627(3) 0.0234(15) Uani 1 1 d .
C11 C 0.1144(10) 0.2102(6) 0.3065(3) 0.0225(14) Uani 1 1 d .
H11 H 0.0790 0.1850 0.2639 0.027 Uiso 1 1 calc R
C12 C 0.0648(10) 0.3457(6) 0.3156(3) 0.0222(14) Uani 1 1 d .
H12 H -0.0076 0.4159 0.2791 0.027 Uiso 1 1 calc R
C13 C 0.2806(11) 0.8695(7) 0.1245(4) 0.0249(16) Uani 1 1 d .
C14 C 0.2203(10) 0.7792(7) 0.1817(3) 0.0225(15) Uani 1 1 d .
H14 H 0.1616 0.8129 0.2216 0.027 Uiso 1 1 calc R
C15 C 0.2449(10) 0.6425(7) 0.1809(3) 0.0233(14) Uani 1 1 d .
H15 H 0.2031 0.5824 0.2207 0.028 Uiso 1 1 calc R
C16 C 0.3309(12) 0.5870(7) 0.1227(4) 0.0277(17) Uani 1 1 d .
C17 C 0.3900(11) 0.6812(7) 0.0652(4) 0.0277(17) Uani 1 1 d .
H17 H 0.4468 0.6486 0.0247 0.033 Uiso 1 1 calc R
C18 C 0.3672(11) 0.8208(7) 0.0663(3) 0.0263(16) Uani 1 1 d .
H18 H 0.4105 0.8821 0.0275 0.032 Uiso 1 1 calc R
C19 C 0.3671(13) 0.4358(7) 0.1223(5) 0.033(2) Uani 1 1 d .
H19A H 0.3185 0.3859 0.1664 0.050 Uiso 1 1 calc R
H19B H 0.4964 0.3989 0.1217 0.050 Uiso 1 1 calc R
H19C H 0.3101 0.4238 0.0784 0.050 Uiso 1 1 calc R
C20 C 0.7270(11) 0.1293(7) 0.3751(3) 0.0239(15) Uani 1 1 d .
C21 C 0.7822(11) 0.1707(7) 0.4353(3) 0.0219(15) Uani 1 1 d .
H21 H 0.8579 0.1053 0.4726 0.026 Uiso 1 1 calc R
C22 C 0.7260(11) 0.3096(7) 0.4410(4) 0.0273(16) Uani 1 1 d .
H22 H 0.7628 0.3372 0.4826 0.033 Uiso 1 1 calc R
C23 C 0.6167(12) 0.4075(7) 0.3862(4) 0.0299(17) Uani 1 1 d .
C24 C 0.5667(11) 0.3629(7) 0.3242(3) 0.0269(15) Uani 1 1 d .
H24 H 0.4956 0.4283 0.2854 0.032 Uiso 1 1 calc R
C25 C 0.6199(11) 0.2262(7) 0.3194(3) 0.0248(15) Uani 1 1 d .
H25 H 0.5835 0.1978 0.2780 0.030 Uiso 1 1 calc R
C26 C 0.5554(13) 0.5575(9) 0.3907(5) 0.041(2) Uani 1 1 d .
H26A H 0.4800 0.6091 0.3474 0.061 Uiso 1 1 calc R
H26B H 0.6597 0.5974 0.3925 0.061 Uiso 1 1 calc R
H26C H 0.4865 0.5632 0.4352 0.061 Uiso 1 1 calc R
loop_
_atom_site_aniso_label
_atom_site_aniso_U_11
_atom_site_aniso_U_22
_atom_site_aniso_U_33
_atom_site_aniso_U_23
_atom_site_aniso_U_13
_atom_site_aniso_U_12
I1 0.0278(6) 0.0173(3) 0.0281(3) -0.0060(2) 0.0003(2) -0.0038(2)
I2 0.0250(5) 0.0160(3) 0.0261(3) -0.0056(2) 0.0023(2) -0.0030(2)
S1 0.0312(13) 0.0173(8) 0.0238(8) -0.0038(6) 0.0045(7) -0.0006(7)
S2 0.0347(13) 0.0154(8) 0.0257(8) -0.0052(6) -0.0046(7) -0.0032(7)
O6 0.119(8) 0.024(3) 0.024(2) -0.009(2) -0.005(3) 0.013(3)
O5 0.038(5) 0.019(3) 0.093(5) -0.006(3) -0.019(4) -0.007(3)
O4 0.035(4) 0.013(2) 0.035(3) -0.0026(18) -0.004(2) 0.006(2)
O3 0.026(3) 0.026(2) 0.032(2) 0.0018(19) 0.006(2) -0.009(2)
O2 0.037(4) 0.020(2) 0.051(3) -0.002(2) 0.009(3) 0.003(2)
O1 0.064(5) 0.025(2) 0.028(2) -0.0086(19) 0.003(2) -0.009(3)
N1 0.025(4) 0.025(3) 0.025(3) -0.006(2) 0.004(2) -0.007(3)
N2 0.037(5) 0.020(3) 0.035(3) -0.006(2) 0.002(3) -0.004(3)
N4 0.022(4) 0.022(3) 0.026(3) -0.004(2) 0.003(2) -0.005(2)
N3 0.028(4) 0.017(3) 0.038(3) -0.007(2) -0.002(3) -0.003(2)
C1 0.018(4) 0.017(3) 0.027(3) -0.001(2) -0.001(3) 0.002(3)
C2 0.029(5) 0.024(3) 0.029(3) -0.003(3) 0.001(3) -0.002(3)
C3 0.025(5) 0.020(3) 0.028(3) -0.002(2) -0.001(3) -0.002(3)
C4 0.024(5) 0.018(3) 0.025(3) -0.001(2) -0.003(3) 0.001(3)
C5 0.030(5) 0.027(3) 0.023(3) -0.008(2) 0.005(3) 0.000(3)
C6 0.038(5) 0.022(3) 0.024(3) 0.000(2) 0.003(3) -0.012(3)
C7 0.021(4) 0.013(3) 0.025(3) -0.002(2) 0.003(2) 0.002(2)
C8 0.023(5) 0.023(3) 0.024(3) -0.006(2) -0.001(3) -0.003(3)
C9 0.024(5) 0.023(3) 0.023(3) 0.000(2) 0.002(3) -0.001(3)
C10 0.032(5) 0.015(3) 0.024(3) -0.004(2) 0.000(3) -0.007(3)
C11 0.022(4) 0.016(3) 0.028(3) -0.005(2) -0.002(3) -0.001(3)
C12 0.024(4) 0.014(3) 0.025(3) -0.001(2) -0.003(3) -0.001(3)
C13 0.025(5) 0.018(3) 0.026(3) -0.001(2) -0.004(3) 0.004(3)
C14 0.020(5) 0.023(3) 0.025(3) -0.008(2) 0.004(3) -0.004(3)
C15 0.016(4) 0.024(3) 0.026(3) 0.001(2) 0.000(2) -0.002(3)
C16 0.028(5) 0.022(3) 0.032(3) -0.009(3) -0.006(3) -0.002(3)
C17 0.028(5) 0.023(3) 0.028(3) -0.007(2) 0.003(3) 0.004(3)
C18 0.030(5) 0.026(3) 0.020(3) -0.002(2) -0.003(3) -0.003(3)
C19 0.036(6) 0.013(3) 0.051(4) -0.011(3) 0.010(4) -0.001(3)
C20 0.024(5) 0.019(3) 0.026(3) 0.000(2) 0.000(3) -0.003(3)
C21 0.026(5) 0.021(3) 0.020(2) -0.007(2) 0.006(3) -0.007(3)
C22 0.023(5) 0.030(3) 0.033(3) -0.013(3) 0.004(3) -0.008(3)
C23 0.021(5) 0.026(4) 0.045(4) -0.011(3) 0.013(3) -0.007(3)
C24 0.019(5) 0.024(3) 0.033(3) 0.000(2) 0.005(3) -0.001(3)
C25 0.026(5) 0.026(3) 0.020(3) -0.002(2) 0.003(3) -0.005(3)
C26 0.029(6) 0.026(4) 0.072(6) -0.025(4) 0.011(4) -0.004(3)
loop_
_atom_type_symbol
_atom_type_description
_atom_type_scat_dispersion_real
_atom_type_scat_dispersion_imag
_atom_type_scat_source
C C -0.0020 0.0021 'International Tables Vol C Tables 4.2.6.8 and 6.1.1.4'
H H 0.0000 0.0000 'International Tables Vol C Tables 4.2.6.8 and 6.1.1.4'
N N -0.0031 0.0043 'International Tables Vol C Tables 4.2.6.8 and 6.1.1.4'
O O -0.0042 0.0079 'International Tables Vol C Tables 4.2.6.8 and 6.1.1.4'
S S 0.1265 0.1577 'International Tables Vol C Tables 4.2.6.8 and 6.1.1.4'
I I -0.6948 2.2434 'International Tables Vol C Tables 4.2.6.8 and 6.1.1.4'
loop_
_geom_angle_atom_site_label_1
_geom_angle_atom_site_label_2
_geom_angle_atom_site_label_3
_geom_angle
O3 S1 O1 112.7(4)
O3 S1 O2 112.9(3)
O1 S1 O2 113.6(4)
O3 S1 C13 106.3(3)
O1 S1 C13 105.4(3)
O2 S1 C13 105.1(4)
O5 S2 O6 114.4(5)
O5 S2 O4 111.8(4)
O6 S2 O4 111.9(4)
O5 S2 C20 104.6(4)
O6 S2 C20 106.0(3)
O4 S2 C20 107.5(3)
N2 N1 C4 178.5(6)
N3 N4 C10 178.8(7)
C6 C1 C2 121.8(6)
C6 C1 I1 120.2(5)
C2 C1 I1 117.8(5)
C3 C2 C1 119.2(7)
C4 C3 C2 116.7(6)
N1 C4 C3 116.9(6)
N1 C4 C5 118.6(6)
C3 C4 C5 124.5(6)
C6 C5 C4 118.0(6)
C5 C6 C1 119.8(6)
C8 C7 C12 123.1(6)
C8 C7 I2 119.5(5)
C12 C7 I2 117.4(4)
C7 C8 C9 118.7(6)
C8 C9 C10 117.4(6)
N4 C10 C11 117.7(6)
N4 C10 C9 117.7(6)
C11 C10 C9 124.6(6)
C12 C11 C10 116.8(6)
C11 C12 C7 119.4(6)
C18 C13 C14 120.2(6)
C18 C13 S1 119.8(5)
C14 C13 S1 120.0(5)
C15 C14 C13 120.3(6)
C14 C15 C16 122.1(6)
C15 C16 C17 116.4(6)
C15 C16 C19 123.1(7)
C17 C16 C19 120.4(7)
C18 C17 C16 121.6(6)
C13 C18 C17 119.3(6)
C21 C20 C25 120.0(6)
C21 C20 S2 120.5(5)
C25 C20 S2 119.5(5)
C20 C21 C22 119.9(6)
C23 C22 C21 120.9(6)
C22 C23 C24 118.0(6)
C22 C23 C26 121.9(7)
C24 C23 C26 120.1(7)
C25 C24 C23 121.0(6)
C24 C25 C20 120.2(6)
loop_
_geom_bond_atom_site_label_1
_geom_bond_atom_site_label_2
_geom_bond_distance
I1 C1 2.094(7)
I2 C7 2.111(6)
S1 O3 1.449(5)
S1 O1 1.457(5)
S1 O2 1.467(7)
S1 C13 1.781(7)
S2 O5 1.440(7)
S2 O6 1.457(6)
S2 O4 1.455(6)
S2 C20 1.775(7)
N1 N2 1.111(8)
N1 C4 1.375(9)
N4 N3 1.094(8)
N4 C10 1.394(8)
C1 C6 1.398(10)
C1 C2 1.409(10)
C2 C3 1.396(9)
C3 C4 1.393(10)
C4 C5 1.403(9)
C5 C6 1.355(9)
C7 C8 1.378(9)
C7 C12 1.400(9)
C8 C9 1.393(9)
C9 C10 1.405(9)
C10 C11 1.387(9)
C11 C12 1.390(8)
C13 C18 1.387(10)
C13 C14 1.388(9)
C14 C15 1.360(9)
C15 C16 1.416(10)
C16 C17 1.409(11)
C16 C19 1.499(9)
C17 C18 1.393(9)
C20 C21 1.390(9)
C20 C25 1.394(9)
C21 C22 1.405(9)
C22 C23 1.395(11)
C23 C24 1.423(11)
C23 C26 1.510(11)
C24 C25 1.378(9)
